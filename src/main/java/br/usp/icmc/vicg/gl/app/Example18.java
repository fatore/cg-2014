package br.usp.icmc.vicg.gl.app;

import java.awt.Frame;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;

import javax.media.opengl.GL;
import javax.media.opengl.GL3;
import javax.media.opengl.GLAutoDrawable;
import javax.media.opengl.GLCapabilities;
import javax.media.opengl.GLEventListener;
import javax.media.opengl.GLProfile;
import javax.media.opengl.awt.GLCanvas;

import br.usp.icmc.vicg.gl.util.Shader;
import br.usp.icmc.vicg.gl.util.ShaderFactory;
import br.usp.icmc.vicg.gl.util.ShaderFactory.ShaderType;
import com.jogamp.common.nio.Buffers;

import com.jogamp.opengl.util.AnimatorBase;
import com.jogamp.opengl.util.FPSAnimator;
import com.jogamp.opengl.util.awt.ImageUtil;
import com.jogamp.opengl.util.texture.awt.AWTTextureIO;
import java.awt.image.BufferedImage;
import java.io.IOException;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.imageio.ImageIO;

public class Example18 implements GLEventListener {

  private final Shader shader; // Gerenciador dos shaders

  private int time_handle;
  private int resolution_handle;
  private int tex0_handle;
  private int position_handle;

  private com.jogamp.opengl.util.texture.Texture texturedata;

  private int vao[];

  private final float xres = 400;
  private final float yres = 400;
  private int time;

  public Example18() {
    // Carrega os shaders
    shader = ShaderFactory.getInstance(ShaderType.PLANET_SHADER);
  }

  @Override
  public void init(GLAutoDrawable drawable) {
    try {
      // Get pipeline
      GL3 gl = drawable.getGL().getGL3();

      // Print OpenGL version
      System.out.println("OpenGL Version: " + gl.glGetString(GL.GL_VERSION) + "\n");

      gl.glClearColor(0.0f, 0.0f, 0.0f, 1.0f);
      gl.glClearDepth(1.0f);

      gl.glEnable(GL.GL_DEPTH_TEST);
      gl.glDisable(GL.GL_CULL_FACE);

      //inicializa os shaders
      shader.init(gl);

      //ativa os shaders
      shader.bind();
      
      //create shader variables
      position_handle = shader.getAttribLocation("pos");
      time_handle = shader.getUniformLocation("time");
      resolution_handle = shader.getUniformLocation("resolution");
      tex0_handle = shader.getUniformLocation("tex0");
            
      //creating VAO
      vao = new int[1];
      gl.glGenVertexArrays(1, vao, 0);
      gl.glBindVertexArray(vao[0]);

      // create vertex positions buffer
      float vertices[] = new float[]{-1.0f, -1.0f, 1.0f, 
        -1.0f, -1.0f, 1.0f, 1.0f, -1.0f, 1.0f, 1.0f, -1.0f, 1.0f};

      int vbo[] = new int[3];
      gl.glGenBuffers(1, vbo, 0);
      gl.glBindBuffer(GL.GL_ARRAY_BUFFER, vbo[0]); // Bind normals buffer
      gl.glBufferData(GL3.GL_ARRAY_BUFFER, vertices.length * Buffers.SIZEOF_FLOAT,
              Buffers.newDirectFloatBuffer(vertices), GL3.GL_STATIC_DRAW);
      gl.glEnableVertexAttribArray(position_handle);
      gl.glVertexAttribPointer(position_handle, 2, GL3.GL_FLOAT, false, 0, 0);
      gl.glBindBuffer(GL3.GL_ARRAY_BUFFER, 0);
      gl.glUniform1i(position_handle, 0);
            
      //load texture
      BufferedImage image = ImageIO.read(getClass().getClassLoader().getResourceAsStream("images/tex4.jpg"));
      //ImageUtil.flipImageVertically(image);
      texturedata = AWTTextureIO.newTexture(GLProfile.get(GLProfile.GL3), image, true);
      texturedata.setTexParameteri(gl, GL.GL_TEXTURE_MIN_FILTER, GL.GL_LINEAR);
      texturedata.setTexParameteri(gl, GL.GL_TEXTURE_MAG_FILTER, GL.GL_LINEAR);
      //texturedata.setTexParameteri(gl, GL.GL_TEXTURE_MIN_FILTER, GL.GL_LINEAR_MIPMAP_LINEAR);
      texturedata.setTexParameteri(gl, GL.GL_TEXTURE_WRAP_S, GL.GL_REPEAT);
      texturedata.setTexParameteri(gl, GL.GL_TEXTURE_WRAP_T, GL.GL_REPEAT);
      gl.glGenerateMipmap(GL3.GL_TEXTURE_2D);
      
      //activiate texture
      gl.glUniform1f(tex0_handle, 0);
    } catch (IOException ex) {
      Logger.getLogger(Example18.class.getName()).log(Level.SEVERE, null, ex);
    }
  }

  @Override
  public void display(GLAutoDrawable drawable) {
    // Recupera o pipeline
    GL3 gl = drawable.getGL().getGL3();

    // Limpa o frame buffer com a cor definida
    gl.glClear(GL3.GL_COLOR_BUFFER_BIT | GL3.GL_DEPTH_BUFFER_BIT);

    gl.glViewport(0, 0, (int) xres, (int) yres);

    gl.glUniform1f(time_handle, (++time)/10);
    gl.glUniform2f(resolution_handle, xres, yres);
        
    gl.glActiveTexture(GL3.GL_TEXTURE0);
    texturedata.bind(gl);

    gl.glBindVertexArray(vao[0]);
    gl.glDrawArrays(GL3.GL_TRIANGLES, 0, 6);
    gl.glBindVertexArray(0);
    
    gl.glFlush();
  }

  @Override
  public void reshape(GLAutoDrawable drawable, int x, int y, int width, int height) {
  }

  @Override
  public void dispose(GLAutoDrawable drawable) {
  }

  public static void main(String[] args) {
    // Get GL3 profile (to work with OpenGL 4.0)
    GLProfile profile = GLProfile.get(GLProfile.GL3);

    // Configurations
    GLCapabilities glcaps = new GLCapabilities(profile);
    glcaps.setDoubleBuffered(true);
    glcaps.setHardwareAccelerated(true);

    // Create canvas
    GLCanvas glCanvas = new GLCanvas(glcaps);

    // Add listener to panel
    Example18 listener = new Example18();
    glCanvas.addGLEventListener(listener);

    Frame frame = new Frame("Example 05");
    frame.setSize(400, 400);
    frame.add(glCanvas);
    final AnimatorBase animator = new FPSAnimator(glCanvas, 60);

    frame.addWindowListener(new WindowAdapter() {
      @Override
      public void windowClosing(WindowEvent e) {
        new Thread(new Runnable() {
          @Override
          public void run() {
            animator.stop();
            System.exit(0);
          }
        }).start();
      }
    });
    frame.setVisible(true);
    animator.start();
  }
}
